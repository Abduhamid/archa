<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Color;
use App\Models\Material;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $colors = Color::where('id','>', 1)->paginate(25);
        $data = ['title' => 'Цвет', 'user' => $request->user(), 'activ' => self::activColor, 'models' => $colors,];
        return view('admin.color', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('color.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('name') && $request->input('name') != null && request()->ajax()) {
            $color = new Color();
            $color->name = $request->input('name');
            $color->save();
            return response()->json($color);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Color $color
     * @return \Illuminate\Http\Response
     */
    public function show(Color $color)
    {
        return redirect()->route('color.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Color $color
     * @return \Illuminate\Http\Response
     */
    public function edit(Color $color)
    {
        return redirect()->route('color.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Color $color
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Color $color)
    {
        if ($request->has('name') && $request->input('name') != null && request()->ajax()) {
            $color->name = $request->input('name');
            $color->update();
            return response()->json($color);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Color $color
     * @return \Illuminate\Http\Response
     */
    public function destroy(Color $color)
    {
        $id=$color->id;
        $color->delete();
        return response()->json($id);
    }

    public function table($object, $limit=10)
    {
        $table=null;
        if (is_object($object)){
            $table='<table>';
            $attributes=array_keys($object::first()->toArray());
            $header='<tr>';
            foreach ($attributes as $item){
                $header.='<td>'.$item.'</td>';
            }
            $header.='</tr>';
            $table.=$header;
            $models=$object::all()->take($limit);

            foreach ($models as $model){
                $body='<tr>';
                foreach ($attributes as $item){
                    $body.='<td>'.$model->$item.'</td>';
                }
                $body.='</tr>';
                $table.=$body;
            }
            $table.='</table>';

        }
        return $table;
    }
}
