<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Detail;
use App\Models\OrderItem;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;

class DetailController extends Controller
{

    public function detail(Request $request, OrderItem $orderItem)
    {
        if ($request->hasFile('excel')) {
            $path = $request->file('excel')->getRealPath();
            $file = $request->file('excel');
            if ($file->getClientOriginalExtension() == 'xlsx' || $file->getClientOriginalExtension() == 'xls') {
                $reader = null;
                if ($file->getClientOriginalExtension() == 'xlsx') {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                }
                $user = User::all()->first();
                $spreadsheet = $reader->load($path);
                $spreadsheet->getActiveSheet()->getCell('B4');
                $data = [
                    'order' => $spreadsheet->getActiveSheet()->getCell('B4')->getValue(),
                    'developer' => $spreadsheet->getActiveSheet()->getCell('B8')->getValue(),
                ];
                $id = 12;
                $i = 1;
                while (true) {
                    if ($spreadsheet->getActiveSheet()->getCell('B' . $id)->getValue() != null &&
                        $spreadsheet->getActiveSheet()->getCell('C' . $id)->getValue() != null && $i == 1) {
                        $detail = new Detail();
                        $detail->order = $data['order'];
                        $detail->user_id = $user->id;
                        $detail->orderitem_id = $orderItem->id;
                        $detail->developer = $data['developer'];
                        $detail->vendorcode = $spreadsheet->getActiveSheet()->getCell('A' . $id)->getValue();
                        $detail->name = $spreadsheet->getActiveSheet()->getCell('B' . $id)->getValue();
                        $detail->unitofme = $spreadsheet->getActiveSheet()->getCell('C' . $id)->getValue();
                        $detail->estimatedamount = $spreadsheet->getActiveSheet()->getCell('D' . $id)->getValue();
                        $detail->coefficent = $spreadsheet->getActiveSheet()->getCell('E' . $id)->getValue();
                        $detail->quantityinproduct = $spreadsheet->getActiveSheet()->getCell('F' . $id)->getValue();
                        $detail->quantityinorder = $spreadsheet->getActiveSheet()->getCell('G' . $id)->getValue();
                        $detail->price = $spreadsheet->getActiveSheet()->getCell('H' . $id)->getValue();
                        $detail->costinproduct = $spreadsheet->getActiveSheet()->getCell('I' . $id)->getValue();
                        $detail->costvalue = $spreadsheet->getActiveSheet()->getCell('J' . $id)->getValue();
                        $detail->note = $spreadsheet->getActiveSheet()->getCell('K' . $id)->getValue();
                        $detail->save();
                    } elseif ($spreadsheet->getActiveSheet()->getCell('B' . $id)->getValue() == null &&
                        $spreadsheet->getActiveSheet()->getCell('C' . $id)->getValue() == null && $i == 1) {
                        $i--;
                        $detail = new Detail();
                        $detail->user_id = $user->id;
                        $detail->orderitem_id = $orderItem->id;
                        $detail->order = $data['order'];
                        $detail->developer = $data['developer'];
                        $detail->costinproduct = $spreadsheet->getActiveSheet()->getCell('I' . $id)->getValue();
                        $detail->costvalue = $spreadsheet->getActiveSheet()->getCell('J' . $id)->getValue();
                        $detail->save();
                        $detail = null;
                    } elseif ($i == 0) {
                        break;
                    }
                    $id++;
                }

            }
        }
        return redirect()->route('orderItem.show', $orderItem->id);
    }
}
