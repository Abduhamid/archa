<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Material;
use App\Models\MaterialColor;
use Illuminate\Http\Request;

class MaterialColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $materialColors = MaterialColor::with('material')->paginate(25);
        $materials=Material::all();
        $data = ['title' => 'Цвет материала', 'user' => $request->user(), 'activ' => self::activMaterialColor, 'models' => $materialColors, 'material'=>$materials];
        return view('admin.materialColor', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('materialColor.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('material') && $request->has('name') && $request->input('name') != null && request()->ajax()) {
            $materialColor = new MaterialColor();
            $materialColor->name = $request->input('name');
            $materialColor->material_id=$request->input('material');
            $materialColor->save();
            $materialColor=$materialColor->load('material');
            $data=['id'=>$materialColor->id,'name'=>$materialColor->name,'material_id'=>$materialColor->material_id,'material_name'=>$materialColor->material->name];
            return response()->json($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MaterialColor  $materialColor
     * @return \Illuminate\Http\Response
     */
    public function show(MaterialColor $materialColor)
    {
        return redirect()->route('materialColor.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MaterialColor  $materialColor
     * @return \Illuminate\Http\Response
     */
    public function edit(MaterialColor $materialColor)
    {
        return redirect()->route('materialColor.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MaterialColor  $materialColor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MaterialColor $materialColor)
    {
        if ($request->has('material') && $request->has('name') && $request->input('name') != null && request()->ajax()) {
            $materialColor->name = $request->input('name');
            $materialColor->material_id=$request->input('material');
            $materialColor->update();
            $materialColor=$materialColor->load('material');
            $data=['id'=>$materialColor->id,'name'=>$materialColor->name,'material_id'=>$materialColor->material_id,'material_name'=>$materialColor->material->name];
            return response()->json($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MaterialColor  $materialColor
     * @return \Illuminate\Http\Response
     */
    public function destroy(MaterialColor $materialColor)
    {
        $id=$materialColor->id;
        $materialColor->delete();
        return response()->json($id);
    }
}
