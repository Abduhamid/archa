<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Color;
use App\Models\MaterialColor;
use App\Models\Order;
use App\Models\Product;
use App\Models\Style;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $order = Order::paginate(25);
        $data = ['title' => 'Заказ', 'user' => $request->user(), 'activ' => self::activOrder, 'models' => $order,];
        return view('admin.order', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('order.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (request()->ajax()) {
            $order = new Order();
            $order = $order->attribute($request, $order);
            $order->save();
            return response()->json($order->dataJson($order));
        }
        return response()->json(false);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Order $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order, Request $request)
    {
        $order->load(['orderItems', 'orderItems.style', 'orderItems.materialColor', 'orderItems.product', 'orderItems.materialColor.material']);
        $models = $order->orderItems;
        $product = Product::all()->load(['style', 'materialColor', 'materialColor.material']);
        $materialColor=MaterialColor::all()->load('material');
        $style=Style::all();
        $color=Color::all();
        $data = [
            'title' => 'Заказ',
            'user' => $request->user(),
            'activ' => self::activOrder,
            'models' => $models,
            'product' => $product,
            'order' => $order,
            'colors' => $color,
            'style' => $style,
            'materialColors' => $materialColor,
        ];
        return view('admin.orderItem', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Order $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return redirect()->route('order.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Order $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        if (request()->ajax()) {
            $order = $order->attribute($request, $order);
            $order->update();
            return response()->json($order->dataJson($order));
        }
        return response()->json(false);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Order $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $id = $order->id;
        $order->delete();
        return response()->json($id);
    }
}
