<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OrderExport;
use App\Http\Controllers\Controller;
use App\Models\Color;
use App\Models\Detail;
use App\Models\MaterialColor;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\Style;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PDF;


class OrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return redirect()->route('order.index');
        $order = Order::all();
        $data = ['title' => 'Заказ', 'user' => $request->user(), 'activ' => self::activOrder, 'models' => $order,];
        return view('admin.order', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('order.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (request()->ajax() && $request->has('order_id')) {
            $orderItem = new OrderItem();
            $orderItem->store($request, $orderItem);
            $orderItem->order_id = $request->input('order_id');
            $orderItem->save();
            return response()->json($orderItem->dataJson($orderItem));
        }
    }


    /**
     * Display the specified resource.
     *
     * @param \App\Models\OrderItem $orderItem
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, OrderItem $orderItem)
    {
        $order=$orderItem->order;
        $orderItem->load(['style', 'materialColor', 'product', 'materialColor.material', 'order', 'details']);
        $detail = null;
        $detail=Detail::all()->where('orderitem_id','=', $orderItem->id);
        $details=$detail->first();
        $data = [
            'title' => 'Заказ',
            'user' => $request->user(),
            'activ' => self::activOrder,
            'orderItem' => $orderItem,
            'order' => $order,
            'detail' => $detail,
            'details' => $details,
        ];
        return view('admin.orderItemShow', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\OrderItem $orderItem
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderItem $orderItem)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\OrderItem $orderItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderItem $orderItem)
    {
        if (request()->ajax() && $request->has('order_id')) {
            $orderItem->store($request, $orderItem);
            $orderItem->update();
            return response()->json($orderItem->dataJson($orderItem));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\OrderItem $orderItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderItem $orderItem)
    {
        $id = $orderItem->id;
        $orderItem->delete();
        return response()->json($id);
    }

    public function excelExport(Order $order)
    {
        $order->load(['orderItems.style', 'orderItems.materialColor', 'orderItems.product', 'orderItems.materialColor.material']);
        $orderItems = $order->orderItems;
        $sum = $orderItems->sum('count');
        $count = $orderItems->count();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $spreadsheet = $reader->load("newOrder.xls");
        $spreadsheet->getActiveSheet()->getCell('H6')->setValue($order->number);
        $spreadsheet->getActiveSheet()->getCell('E8')->setValue($order->fio);
        $spreadsheet->getActiveSheet()->getCell('E10')->setValue($order->phoneNumber);
        $spreadsheet->getActiveSheet()->getCell('E12')->setValue($order->adress);
        $spreadsheet->getActiveSheet()->getCell('K8')->setValue(date('d.m.Y', strtotime($order->date)));
        $spreadsheet->getActiveSheet()->getCell('K10')->setValue(date('d.m.Y', strtotime($order->srok)));
        $spreadsheet->getActiveSheet()->getCell('K12')->setValue($order->dateSrok . ' дней');
        $spreadsheet->getActiveSheet()->getCell('H14')->setValue($order->name);
        $spreadsheet->getActiveSheet()->getCell('M18')->setValue($sum);
        if ($count >= 2) {
            $spreadsheet->getActiveSheet()->insertNewRowBefore(18, $count - 1);
        }
        foreach ($orderItems as $k => $item) {
            $key = $k + 17;
            $spreadsheet->getActiveSheet()->getCell('C' . $key)->setValue($k + 1);
            $spreadsheet->getActiveSheet()->getCell('D' . $key)->setValue($item->name);
            $spreadsheet->getActiveSheet()->getCell('E' . $key)->setValue($item->length);
            $spreadsheet->getActiveSheet()->getCell('F' . $key)->setValue($item->width);
            $spreadsheet->getActiveSheet()->getCell('G' . $key)->setValue($item->height);
            $spreadsheet->getActiveSheet()->getCell('H' . $key)->setValue($item->glubina);
            $spreadsheet->getActiveSheet()->getCell('I' . $key)->setValue($item->style->name);
            $spreadsheet->getActiveSheet()->getCell('J' . $key)->setValue($item->materialColor->material->name);
            $spreadsheet->getActiveSheet()->getCell('K' . $key)->setValue($item->materialColor->name);
            $spreadsheet->getActiveSheet()->getCell('L' . $key)->setValue(($item->color->name == 1) ? '' : $item->color->name);
            $spreadsheet->getActiveSheet()->getCell('M' . $key)->setValue($item->count);
        }
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save('export.xlsx');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="export.xlsx"');
        $writer->save("php://output");
        exit;
    }
}
