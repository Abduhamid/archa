<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\Color;
use App\Models\MaterialColor;
use App\Models\Product;
use App\Models\Style;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $product = Product::with(['style', 'materialColor', 'materialColor.material'])->paginate(25);
        $materialColor = MaterialColor::all()->load('material');
        $style = Style::all();
        $data = ['title' => 'Продукт', 'user' => $request->user(), 'activ' => self::activProduct, 'models' => $product,
            'materialColors' => $materialColor, 'style' => $style];
        return view('admin.product', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('product.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (request()->ajax()) {
            $product = new Product();
            if ($product->store($request, $product) != null) {
                return response()->json($product->dataJson($product));
            }
        }
        return response()->json(false);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        if (request()->ajax()) {
            return response()->json($product->dataJson($product));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return redirect()->route('product.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        if (request()->ajax()) {
            if ($product->updateAttribute($request, $product) != null) {
                return response()->json($product->dataJson($product));
            }
        }
        return response()->json(false);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $id = $product->id;
        $product->delete();
        return response()->json($id);
    }
}
