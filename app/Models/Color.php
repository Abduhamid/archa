<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table='color';
    protected $primaryKey='id';

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'color_id','id');
    }
}
