<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table='details';
    protected $primaryKey='id';

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class, 'orderitem_id','id');
    }

}
