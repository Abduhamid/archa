<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table='material';
    protected $primaryKey='id';

    public function materialColors()
    {
        return $this->hasMany(MaterialColor::class, 'material_id', 'id');
    }
}
