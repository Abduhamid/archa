<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialColor extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'materialcolor';

    public function material()
    {
        return $this->belongsTo(Material::class, 'material_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'materialcolor_id', 'id');
    }

    public function orderItem()
    {
        return $this->hasMany(OrderItem::class, 'materialcolor_id', 'id');
    }
}
