<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='order';
    protected $primaryKey='id';

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'order_id','id');
    }

    public function attribute($data, $object)
    {
        if ($data->has('fio') && $data->has('name') && $data->has('adress') && $data->has('date') &&
            $data->has('srok') && $data->has('number') && $data->has('phoneNumber') && $data->has('dateEnd'))
        {
            $input = $data->input();
            $object->name = $input['name'];
            $object->fio = $input['fio'];
            $object->adress = $input['adress'];
            $object->date = $input['date'];
            $object->srok = $input['srok'];
            $object->number = $input['number'];
            $object->datesrok = $input['dateEnd'];
            $object->phoneNumber = $input['phoneNumber'];
            $user=User::all();
            if (count($user)>=1){
                $object->user_id=$user[0]->id;
            }
            return $object;
        }
        return null;
    }

    public function dataJson($object)
    {
        $data = [
            'id' => $object->id,
            'name' => $object->name,
            'number' => $object->number,
            'fio' => $object->fio,
            'phonenumber' => $object->phoneNumber,
            'date' => $object->date,
            'srok' => $object->srok,
            'adress' => $object->adress,
            'dateend' => $object->datesrok,
            'http'=>route('order.show', $object->id),
        ];
        return $data;
    }
}
