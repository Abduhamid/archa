<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table='orderitem';
    protected $primaryKey='id';

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id','id');
    }
    public function style()
    {
        return $this->belongsTo(Style::class, 'style_id', 'id');
    }

    public function color()
    {
        return $this->belongsTo(Color::class, 'color_id', 'id');
    }

    public function materialColor()
    {
        return $this->belongsTo(MaterialColor::class, 'materialcolor_id', 'id');
    }

    public function details()
    {
        return $this->hasMany(Detail::class,'orderitem_id','id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id','id');
    }

    public function store($data, $object)
    {
        if ($data->has('height') && $data->has('name') && $data->has('length') && $data->has('width') &&
            $data->has('glubina') && $data->has('style') && $data->has('materialColor') && $data->has('order_id')
            && $data->has('color') && $data->has('product') && $data->has('count')) {
            $input = $data->input();
            $object->name = $input['name'];
            $object->length = $input['length'];
            $object->height = $input['height'];
            $object->width = $input['width'];
            $object->glubina = $input['glubina'];
            $object->style_id = $input['style'];
            $object->color_id = $input['color'];
            $object->count = $input['count'];
            $object->product_id = $input['product'];
            $object->materialcolor_id = $input['materialColor'];
            return $object;
        }
        return null;
    }

    public function dataJson($object)
    {
        $object = $object->load(['style', 'materialColor', 'materialColor.material', 'product', 'color']);
        $data = [
            'id' => $object->id,
            'name' => $object->name,
            'length' => $object->length,
            'height' => $object->height,
            'width' => $object->width,
            'glubina' => $object->glubina,
            'style_name' => $object->style->name,
            'style_id' => $object->style->id,
            'color' => ($object->color->name==1)?'':$object->color->name,
            'color_id' => $object->color->id,
            'materialColor_name' => $object->materialColor->name,
            'materialColor_id' => $object->materialColor->id,
            'material_name' => $object->materialColor->material->name,
            'count'=>$object->count,
            'http'=>route('orderItem.show', $object->id),
        ];
        return $data;
    }
}
