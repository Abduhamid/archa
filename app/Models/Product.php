<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $primaryKey = 'id';

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'product_ic', 'id');
    }

    public function style()
    {
        return $this->belongsTo(Style::class, 'style_id', 'id');
    }

    public function materialColor()
    {
        return $this->belongsTo(MaterialColor::class, 'materialcolor_id', 'id');
    }

    public function store($data, $object)
    {
        if ($data->has('height') && $data->has('name') && $data->has('length') && $data->has('width') &&
            $data->has('glubina') && $data->has('style') && $data->has('materialColor')) {
            $input = $data->input();
            $object->name = $input['name'];
            $object->length = $input['length'];
            $object->height = $input['height'];
            $object->width = $input['width'];
            $object->glubina = $input['glubina'];
            $object->style_id = $input['style'];
            $object->materialcolor_id = $input['materialColor'];
            $object->save();
            return $object;
        }
        return null;
    }

    public function updateAttribute($data, $object)
    {
        if ($data->has('height') && $data->has('name') && $data->has('length') && $data->has('width') &&
            $data->has('glubina') && $data->has('style') && $data->has('materialColor')) {
            $input = $data->input();
            $object->name = $input['name'];
            $object->length = $input['length'];
            $object->height = $input['height'];
            $object->width = $input['width'];
            $object->glubina = $input['glubina'];
            $object->style_id = $input['style'];
            $object->materialcolor_id = $input['materialColor'];
            $object->update();
            return $object;
        }
        return null;
    }

    public function dataJson($object)
    {
        $object = $object->load(['style', 'materialColor', 'materialColor.material']);
        $data = [
            'id' => $object->id,
            'name' => $object->name,
            'length' => $object->length,
            'height' => $object->height,
            'width' => $object->width,
            'glubina' => $object->glubina,
            'style_name' => $object->style->name,
            'style_id' => $object->style->id,
            'materialColor_name' => $object->materialColor->name,
            'materialColor_id' => $object->materialColor->id,
            'material_name' => $object->materialColor->material->name,
        ];
        return $data;
    }


}
