<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    protected $primaryKey='id';
    protected $table='style';

    public function products()
    {
        return $this->hasMany(Product::class, 'style_id','id');
    }
    public function orderItems()
    {
        return $this->hasMany( OrderItem::class, 'style_id','id');
    }
}
