<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAttribute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('height');
            $table->bigInteger('length');
            $table->bigInteger('width');
            $table->bigInteger('glubina');
            $table->foreignId('style_id')->constrained()->references('id')->on('style')->onDelete('cascade');
            $table->foreignId('color_id')->constrained()->references('id')->on('color')->onDelete('cascade');
            $table->foreignId('materialcolor_id')->constrained()->references('id')->on('materialcolor')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute');
    }
}
