<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('orderitem_id')->constrained()->references('id')->on('orderitem')->onDelete('cascade');
            $table->foreignId('user_id')->constrained()->references('id')->on('users')->onDelete('cascade');
            $table->string('developer')->nullable();
            $table->string('order')->nullable();
            $table->string('name')->nullable();
            $table->string('vendorcode')->nullable();
            $table->string('unitofme')->nullable();
            $table->string('estimatedamount')->nullable();
            $table->string('coefficent')->nullable();
            $table->string('quantityinproduct')->nullable();
            $table->string('quantityinorder')->nullable();
            $table->string('price')->nullable();
            $table->string('costinproduct')->nullable();
            $table->string('costvalue')->nullable();
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
