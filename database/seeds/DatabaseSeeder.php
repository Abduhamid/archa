<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('userstatus')
            ->insert(['name'=>'admin']);
        DB::table('color')
            ->insert(['name'=>'1']);
        DB::table('users')
            ->insert(['name'=>'admin','email'=>'admin@admin.ru','password'=>Hash::make('password'), 'userstatus_id'=>1]);
        // $this->call(UsersTableSeeder::class);
    }
}
