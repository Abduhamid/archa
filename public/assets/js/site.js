jQuery.fn.removeAttributes = function() {
    return this.each(function() {
        var attributes = $.map(this.attributes, function(item) {
            return item.name;
        });
        var img = $(this);
        $.each(attributes, function(i, item) {
            img.removeAttr(item);
        });
    });
};

function closeModal(modal) {
    $('body').removeAttributes();
    modal.removeClass('show');
    $('.modal-backdrop').remove();
    return true;
}

function formReset(form) {
    form.trigger('reset');
    return true;
}
