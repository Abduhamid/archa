<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo">
            <a href="{{route('color.index')}}"><img src="{{asset('assets/images/icon/logo.png')}}" alt="logo"></a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu" id="menu">
{{--                    <li class="{{($activ==1)?'active':''}}">--}}
{{--                        <a href="{{route('color.index')}}" aria-expanded="true"><i class="ti-dashboard"></i><span>Админ</span></a>--}}
                        {{--                        <ul class="collapse">--}}
                        {{--                            <li class="active"><a href="#">Active Menu Item</a></li>--}}
                        {{--                            <li><a href="#">Menuitem 2</a></li>--}}
                        {{--                            <li><a href="#">Menuitem 3</a></li>--}}
                        {{--                        </ul>--}}
{{--                    </li>--}}
                    <li class="{{($activ==1)?'active':''}}"><a href="{{route('color.index')}}"><i class="ti-agenda "></i><span>Цвет</span></a>
                    </li>
                    <li class="{{($activ==2)?'active':''}}"><a href="{{route('material.index')}}"><i class="ti-layers-alt"></i><span>Материал</span></a>
                    </li>
                    <li class="{{($activ==3)?'active':''}}"><a href="{{route('materialColor.index')}}"><i class="ti-tag"></i><span>Цвет материалов</span></a>
                    </li>
                    <li class="{{($activ==4)?'active':''}}"><a href="{{route('style.index')}}"><i class="ti-briefcase"></i><span>Стиль</span></a>
                    </li>
                    <li class="{{($activ==5)?'active':''}}"><a href="{{route('product.index')}}"><i
                                    class="ti-bookmark-alt"></i><span>Продукт</span></a></li>
                    <li class="{{($activ==6)?'active':''}}"><a href="{{route('order.index')}}"><i
                                    class="ti-bookmark-alt"></i><span>Заказ</span></a></li>
                    {{--                    <li>--}}
                    {{--                        <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-align-left"></i>--}}
                    {{--                            <span>Multi--}}
                    {{--                                        level menu</span></a>--}}
                    {{--                        <ul class="collapse">--}}
                    {{--                            <li><a href="#">Item level (1)</a></li>--}}
                    {{--                            <li><a href="#">Item level (1)</a></li>--}}
                    {{--                            <li><a href="#" aria-expanded="true">Item level (1)</a>--}}
                    {{--                                <ul class="collapse">--}}
                    {{--                                    <li><a href="#">Item level (2)</a></li>--}}
                    {{--                                    <li><a href="#">Item level (2)</a></li>--}}
                    {{--                                    <li><a href="#">Item level (2)</a></li>--}}
                    {{--                                </ul>--}}
                    {{--                            </li>--}}
                    {{--                            <li><a href="#">Item level (1)</a></li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                </ul>
            </nav>
        </div>
    </div>
</div>
<!-- sidebar menu area end -->
<!-- main content area start -->
<div class="main-content">
    <!-- page title area start -->
    <div class="page-title-area">
        <div class="row align-items-center">
            <div class="col-md-6 col-sm-6 clearfix">
                <div class="nav-btn pull-left">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="breadcrumbs-area clearfix">
                    <h4 class="page-title pull-left">Админа</h4>
                    <ul class="breadcrumbs pull-left">
{{--                        <li><a href="{{route('color.index')}}">Главная</a></li>--}}
                        <li><span>{{$title}}</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 clearfix">
                <div class="user-profile pull-right">
                    <img class="avatar user-thumb" src="{{asset('assets/images/author/avatar.png')}}" alt="avatar">
                    <h4 class="user-name dropdown-toggle" data-toggle="dropdown">{{$user }} <i
                        ></i></h4>
                    {{--                    class="fa fa-angle-down"--}}
                    {{--                    <div class="dropdown-menu">--}}
                    {{--                        <a class="dropdown-item" href="#">Message</a>--}}
                    {{--                        <a class="dropdown-item" href="#">Settings</a>--}}
                    {{--                        <a class="dropdown-item" href="#">Log Out</a>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>