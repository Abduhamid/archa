@extends('admin.layots.app')
@section('content')
    <div class="row">
        <div class="col-lg-2 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">{{$title}}</h4>
                    <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#added">
                        Добавить
                    </button>
                </div>
            </div>
        </div>
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h4 class="header-title">{{$title}}</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table table-hover progress-table text-center" id="material-crud">
                                <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Название</th>
                                    <th scope="col">Действие</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(is_object($models))
                                    @foreach($models as $item)
                                        <tr id="material_id_{{ $item->id }}">
                                            <th scope="row">{{$item->id}}</th>
                                            <td>{{$item->name}}</td>
                                            <td>
                                                <ul class="d-flex justify-content-center">
                                                    <li class="mr-3"><a class="text-secondary"
                                                                        data-toggle="modal" data-target="#edit"
                                                                        data-id="{{$item->id}}"
                                                                        data-name="{{$item->name}}"><i
                                                                    class="fa fa-edit"></i></a></li>
                                                    <li><a class="text-danger"><i class="ti-trash"
                                                                                  data-toggle="modal"
                                                                                  data-target="#delete"
                                                                                  data-id="{{$item->id}}"
                                                                                  data-name="{{$item->name}}"></i></a>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $models->links() !!}
            </div>
        </div>
    </div>
    <div class="modal fade" id="added" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавление</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times</span></button>
                </div>
                <form method="post" action="{{route('material.store')}}" id="add_form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Имя</label>
                            <input name="name" maxlength="250" class="form-control name" type="text" placeholder="Имя"
                                   id="example-text-input" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-primary" id="btnSave">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <form method="post" action="" id="edit_form">
                    @method('PUT')
                    @csrf
                    <input type="hidden" class="id">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Имя</label>
                            <input name="name" maxlength="250" class="form-control name" type="text" placeholder="Имя"
                                   id="example-text-input" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>
                        <button type="submit" class="btn btn-primary" id="btnEdit">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <form action="{{route('material.index')}}" method="post" id="delete_form">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <p>Вы действительно хотите удалить.</p>
                        <input type="hidden" class="id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>
                        <button type="submit" class="btn btn-primary" id="btnDelete">Да</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#delete').submit(function () {
            var modal = $('#delete');
            var id = modal.find('.id').val();
            var action='{{route('material.index')}}';
            modal.find('form').attr('action', action+'/' + id);
        });
        $('#delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $('#delete');
            modal.find('.id').val(id);
            modal.find('.modal-title').text('Удаление ' + name);
        });
        $('#delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $('#delete');
            modal.find('.id').val(id);
            modal.find('.modal-title').text('Удаление ' + name);
        });
        $('#edit').submit(function () {
            var modal = $('#edit');
            var id = modal.find('.id').val();
            var action='{{route('material.index')}}';
            modal.find('form').attr('action', action+'/' + id);
        });
        $('#edit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $('#edit');
            modal.find('.id').val(id);
            modal.find('.name').val(name);
            modal.find('.modal-title').text('Редактировать ' + name);
        });
    </script>

    <script>
        $(function () {
            $('#btnSave').on('click', function () {
                $('#add_form').ajaxForm({
                    type: "post",
                    resetForm: true,
                    success: function (data) {
                        $('#added').modal('hide');
                        var materialTable = table(data);
                        $('#material-crud').append(materialTable);
                        // formReset($('#add_form'));
                        // closeModal($('#added'));
                    }
                });
            })
        });
        $('#btnEdit').on('click', function () {
            var modal = $('#edit');
            var id = modal.find('.id').val();
            var action='{{route('material.index')}}';
            modal.find('form').attr('action', action+'/' + id);
            $('#edit_form').ajaxForm({
                type: "PUT",
                resetForm: true,
                success: function (data) {
                    $('#edit').modal('hide');
                    var materialTable = table(data);
                    $("#material_id_" + data.id).replaceWith(materialTable);
                }
            }).submit();
        });
        $('#btnDelete').on('click', function () {
            var modal = $('#delete');
            var id = modal.find('.id').val();
            var action='{{route('material.index')}}';
            modal.find('form').attr('action', action+'/' + id);
            $('#delete_form').ajaxForm({
                type: "DELETE",
                resetForm: true,
                success: function (data) {
                    $('#delete').modal('hide');
                    $("#material_id_" + data).remove();
                    // formReset($('#delete_form'));
                }
            }).submit();
        });

        function table(data) {
            var table = '<tr id="material_id_' + data.id + '"><th>' + data.id + '</th><td>' + data.name + '</td>';
            table += '<td> <ul class="d-flex justify-content-center"> <li class="mr-3"><a class="text-secondary" data-toggle="modal" data-target="#edit" data-id="' + data.id + '" data-name="'+data.name + '">';
            table +='<i class="fa fa-edit"></i></a></li> <li><a class="text-danger"><i class="ti-trash"';
            table += 'data-toggle="modal" data-target="#delete" data-id="' + data.id + '" data-name="' + data.name + '"></i></a></li></ul></td>';
            return table;
        }
    </script>
@endsection