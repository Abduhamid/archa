@extends('admin.layots.app')
@section('content')
    <div class="row">
        <div class="col-lg-2 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">{{$title}}</h4>
                    <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#added">
                        Добавить
                    </button>
                </div>
            </div>
        </div>
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h4 class="header-title">{{$title}}</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table table-hover progress-table text-center" id="order-crud">
                                <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">№</th>
                                    <th scope="col">Ф.И.О. заказчика</th>
                                    <th scope="col">Наименование изделия</th>
                                    <th scope="col">Телефон</th>
                                    <th scope="col">Адрес</th>
                                    <th scope="col">Дата оформления заказа</th>
                                    <th scope="col">Дата исполнения заказа</th>
                                    <th scope="col">Срок</th>
                                    <th scope="col">Действие</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(is_object($models))
                                    @foreach($models as $item)
                                        <tr id="order_id_{{ $item->id }}">
                                            <th scope="row">{{$item->number}}</th>
                                            <td>{{$item->fio}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->phoneNumber}}</td>
                                            <td>{{$item->adress}}</td>
                                            <td>{{$item->date}}</td>
                                            <td>{{$item->srok}}</td>
                                            <td>{{$item->dateSrok}}</td>
                                            <td>
                                                <ul class="d-flex justify-content-center">
                                                    <li class="mr-3"><a href="{{route('order.show', $item->id)}}" class="text-secondary"><i
                                                                    class="fa fa-eye"></i></a></li>
                                                    <li class="mr-3"><a class="text-secondary"
                                                                        data-toggle="modal" data-target="#edit"
                                                                        data-id="{{$item->id}}"
                                                                        data-fio="{{$item->fio}}"
                                                                        data-number="{{$item->number}}"
                                                                        data-phonenumber="{{$item->phoneNumber}}"
                                                                        data-adress="{{$item->adress}}"
                                                                        data-date="{{$item->date}}"
                                                                        data-dateend="{{$item->dateSrok}}"
                                                                        data-srok="{{$item->srok}}"
                                                                        data-name="{{$item->name}}"><i
                                                                    class="fa fa-edit"></i></a></li>
                                                    <li><a class="text-danger"><i class="ti-trash"
                                                                                  data-toggle="modal"
                                                                                  data-target="#delete"
                                                                                  data-id="{{$item->id}}"
                                                                                  data-name="{{$item->name}}"></i></a>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $models->links() !!}
            </div>
        </div>
    </div>
    <div class="modal fade" id="added" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавление</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times</span></button>
                </div>
                <form method="post" action="{{route('order.store')}}" id="add_form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Наряд - заказ №</label>
                            <input name="number" max="9999999999" min="0" class="form-control number" type="number" step="any"
                                   placeholder="122"
                                   id="example-text-input" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Наименование изделия</label>
                            <input name="name" maxlength="250" class="form-control name" type="text" placeholder="Наименование изделия"
                                   id="example-text-input" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Ф.И.О. заказчика</label>
                            <input name="fio" maxlength="250" class="form-control fio" type="text" placeholder="Ф.И.О. заказчика"
                                   id="example-text-input" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Адрес</label>
                            <input name="adress" maxlength="250" class="form-control adress" type="text" placeholder="Адрес"
                                   id="example-text-input" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Телефон</label>
                            <input name="phoneNumber"  max="9999999999" min="0" class="form-control phoneNumber" type="number" step="any"
                                   placeholder="92 999 99 29"
                                   id="example-text-input" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="example-date-input" class="col-form-label">Дата оформления заказа</label>
                            <input name="date" class="form-control date" type="date" value="{{date('Y-m-d')}}"
                                   id="example-date-input">
                        </div>
                        <div class="form-group">
                            <label for="example-date-input" class="col-form-label">Дата исполнения заказа</label>
                            <input name="srok" class="form-control srok" type="date" value="{{date('Y-m-d')}}"
                                   id="example-date-input">
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Срок</label>
                            <input name="dateEnd" max="9999999999" min="0" class="form-control dateEnd" type="number" step="any"
                                   placeholder="10"
                                   id="example-text-input" value="" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-primary" id="btnSave">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <form method="post" action="" id="edit_form">
                    @method('PUT')
                    @csrf
                    <input type="hidden" class="id">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Наряд - заказ №</label>
                            <input name="number" max="9999999999" min="0" class="form-control number" type="number" step="any"
                                   placeholder="122"
                                   id="example-text-input" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Наименование изделия</label>
                            <input name="name" maxlength="250" class="form-control name" type="text" placeholder="Наименование изделия"
                                   id="example-text-input" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Ф.И.О. заказчика</label>
                            <input name="fio" maxlength="250" class="form-control fio" type="text" placeholder="Ф.И.О. заказчика"
                                   id="example-text-input" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Адрес</label>
                            <input name="adress" maxlength="250" class="form-control adress" type="text" placeholder="Адрес"
                                   id="example-text-input" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Телефон</label>
                            <input name="phoneNumber" max="9999999999" min="0" class="form-control phoneNumber" type="number" step="any"
                                   placeholder="92 999 99 29"
                                   id="example-text-input" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="example-date-input" class="col-form-label">Дата оформления заказа</label>
                            <input name="date" class="form-control date" type="date" value="{{date('Y-m-d')}}"
                                   id="example-date-input">
                        </div>
                        <div class="form-group">
                            <label for="example-date-input" class="col-form-label">Дата исполнения заказа</label>
                            <input name="srok" class="form-control srok" type="date" value="{{date('Y-m-d')}}"
                                   id="example-date-input">
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Срок</label>
                            <input name="dateEnd" max="9999999999" min="0" class="form-control dateEnd" type="number" step="any"
                                   placeholder="10"
                                   id="example-text-input" value="" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>
                        <button type="submit" class="btn btn-primary" id="btnEdit">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <form action="{{route('order.index')}}" method="post" id="delete_form">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <p>Вы действительно хотите удалить.</p>
                        <input type="hidden" class="id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>
                        <button type="submit" class="btn btn-primary" id="btnDelete">Да</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#delete').submit(function () {
            var modal = $('#delete');
            var id = modal.find('.id').val();
            var action='{{route('order.index')}}';
            modal.find('form').attr('action', action+'/' + id);
        });
        $('#delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $('#delete');
            modal.find('.id').val(id);
            modal.find('.modal-title').text('Удаление ' + name);
        });
        $('#delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $('#delete');
            modal.find('.id').val(id);
            modal.find('.modal-title').text('Удаление ' + name);
        });
        {{--$('#edit').submit(function () {--}}
        {{--    var modal = $('#edit');--}}
        {{--    var id = modal.find('.id').val();--}}
        {{--    var action='{{route('order.index')}}';--}}
        {{--    modal.find('form').attr('action', action+'/' + id);--}}
        {{--});--}}
        $('#edit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $('#edit');
            var fio=button.data('fio');
            var number=button.data('number');
            var phoneNumber=button.data('phonenumber');
            var adress=button.data('adress');
            var dateEnd=button.data('dateend');
            var date=button.data('date');
            var srok=button.data('srok');
            modal.find('.id').val(id);
            modal.find('.name').val(name);
            modal.find('.fio').val(fio);
            modal.find('.phoneNumber').val(phoneNumber);
            modal.find('.adress').val(adress);
            modal.find('.dateEnd').val(dateEnd);
            modal.find('.date').val(date);
            modal.find('.srok').val(srok);
            modal.find('.number').val(number);
            modal.find('.modal-title').text('Редактировать ' + name);
        });
    </script>

    <script>
        $(function () {
            $('#btnSave').on('click', function () {
                $('#add_form').ajaxForm({
                    type: "post",
                    resetForm: true,
                    success: function (data) {
                        $('#added').modal('hide');
                        var orderTable = table(data);
                        $('#order-crud').append(orderTable);
                        // formReset($('#add_form'));
                        // closeModal($('#added'));
                    }
                });
            })
        });
        $('#btnEdit').on('click', function () {
            var modal = $('#edit');
            var id = modal.find('.id').val();
            var action='{{route('order.index')}}';
            modal.find('form').attr('action', action+'/' + id);
            $('#edit_form').ajaxForm({
                type: "PUT",
                resetForm: true,
                success: function (data) {
                    $('#edit').modal('hide');
                    var orderTable = table(data);
                    $("#order_id_" + data.id).replaceWith(orderTable);
                }
            });
        });
        $('#btnDelete').on('click', function () {
            var modal = $('#delete');
            var id = modal.find('.id').val();
            var action='{{route('order.index')}}';
            modal.find('form').attr('action', action+'/' + id);
            $('#delete_form').ajaxForm({
                type: "DELETE",
                resetForm: true,
                success: function (data) {
                    $('#delete').modal('hide');
                    $("#order_id_" + data).remove();
                    // formReset($('#delete_form'));
                }
            });
        });

        function table(data) {
            var table = '<tr id="order_id_' + data.id + '"><th>' + data.number + '</th><td>' + data.fio + '</td>' +
                '<td>' + data.name + '</td><td>' + data.phonenumber + '</td><td>' + data.adress + '</td><td>' + data.date + '</td>' +
                '<td>' + data.srok + '</td><td>' + data.dateend + '</td>';
            table += '<td> <ul class="d-flex justify-content-center">' +
                '<li class="mr-3"><a href="'+data.http+'" class="text-secondary"><i class="fa fa-eye"></i></a></li>' +
                ' <li class="mr-3"><a class="text-secondary" ' +
                'data-toggle="modal" data-target="#edit" data-id="' + data.id + '" data-fio="'+data.fio + '" data-number="'+data.number + '" ' +
                'data-phonenumber="'+data.phonenumber + '" data-adress="'+data.adress + '" data-date="'+data.date + '" ' +
                'data-dateend="'+data.dateend + '" data-srok="'+data.srok + '" data-name="'+data.name + '">';
            table +='<i class="fa fa-edit"></i></a></li> <li><a class="text-danger"><i class="ti-trash"';
            table += 'data-toggle="modal" data-target="#delete" data-id="' + data.id + '" data-name="' + data.name + '"></i></a></li></ul></td>';
            return table;
        }
        // $(document).mouseup(function (e) {
        //     alert(12);
        // })
    </script>
@endsection