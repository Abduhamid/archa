@extends('admin.layots.app')
@section('content')
    <div class="row">
        <div class="col-lg-3 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">{{$title}}</h4>
                    <button type="button" class="btn btn-primary mb-3 " data-toggle="modal" data-target="#added">
                        Добавить
                    </button>
                    <a href="{{route('excelExport', $order->id)}}">
                        <button type="button" class="btn btn-primary mb-3">
                            Скачать файл
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <div class="invoice-area">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="invoice-head">
                            <div class="row">
                                <div class="col-6">
                                    <span>Наряд - заказ № {{$order->number}}</span>
                                    <br>
                                    <span>Наименование изделия</span>
                                    <span>{{$order->name}}</span>
                                </div>
{{--                                <div class="col-6 text-md-right">--}}
{{--                                    <span>Наименование изделия</span>--}}
{{--                                    <span>{{$order->name}}</span>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                        <div class="align-items-center">
                            <div class="row">
                                <div class="col-6">
                                    <p>Ф.И.О. заказчика</p>
                                    <h4>{{$order->fio}}</h4>
                                </div>
                                <div class=" col-6 text-md-right">
                                    <p>Дата оформления заказа</p>
                                    <h4>{{$order->date}}</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="iv-left col-6">
                                    <p>Телефон</p>
                                    <h4>{{$order->phoneNumber}}</h4>
                                </div>
                                <div class="iv-right col-6 text-md-right">
                                    <p>Дата исполнения заказа</p>
                                    <h4>{{$order->srok}}</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="iv-left col-6">
                                    <p>Адресс</p>
                                    <h4>{{$order->adress}}</h4>
                                </div>
                                <div class="iv-right col-6 text-md-right">
                                    <p>Срок</p>
                                    <h4>{{$order->dateSrok}} дней</h4>
                                </div>
                            </div>
                        </div>
{{--                        <h4 class="header-title">{{$title}}</h4>--}}
                        <div class="single-table">
                            <div class="table-responsive">
                                <table class="table table-hover progress-table text-center" id="orderItem-crud">
                                    <thead class="text-uppercase">
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Название</th>
                                        <th scope="col">Длина</th>
                                        <th scope="col">Ширина</th>
                                        <th scope="col">Высота</th>
                                        <th scope="col">Глубина</th>
                                        <th scope="col">Фасон</th>
                                        <th scope="col">Материал</th>
                                        <th scope="col">Цвет материала</th>
                                        <th scope="col">Цвет каркаса</th>
                                        <th scope="col">кол-во</th>
                                        <th scope="col">Действие</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(is_object($models))
                                        @foreach($models as $item)
                                            <tr id="orderItem_id_{{ $item->id }}">
                                                <th scope="row">{{$item->id}}</th>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->length}}</td>
                                                <td>{{$item->width}}</td>
                                                <td>{{$item->height}}</td>
                                                <td>{{$item->glubina}}</td>
                                                <td>{{$item->style->name}}</td>
                                                <td>{{$item->materialColor->material->name}}</td>
                                                <td>{{$item->materialColor->name}}</td>
                                                <td>{{($item->color->name==1)?'':$item->color->name}}</td>
                                                <td>{{$item->count}}</td>
                                                <td>
                                                    <ul class="d-flex justify-content-center">
                                                        <li class="mr-3"><a
                                                                    href="{{route('orderItem.show', $item->id)}}"
                                                                    class="text-secondary"><i
                                                                        class="fa fa-eye"></i></a></li>
                                                        <li class="mr-3"><a class="text-secondary"
                                                                            data-toggle="modal" data-target="#edit"
                                                                            data-id="{{$item->id}}"
                                                                            data-material="{{$item->materialColor->material->id}}"
                                                                            data-materialColor="{{$item->materialColor->id}}"
                                                                            data-style="{{$item->style->id}}"
                                                                            data-color="{{$item->color->id}}"
                                                                            data-product="{{$item->product->id}}"
                                                                            data-name="{{$item->name}}"
                                                                            data-length="{{$item->length}}"
                                                                            data-width="{{$item->width}}"
                                                                            data-height="{{$item->height}}"
                                                                            data-height="{{$item->count}}"
                                                                            data-glubina="{{$item->glubina}}">
                                                                <i class
                                                                   ="fa fa-edit"></i></a></li>
                                                        <li><a class="text-danger"><i class="ti-trash"
                                                                                      data-toggle="modal"
                                                                                      data-target="#delete"
                                                                                      data-id="{{$item->id}}"
                                                                                      data-name="{{$item->name}}"></i></a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="added" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавление</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times</span></button>
                </div>
                <form method="post" action="{{route('orderItem.store')}}" id="add_form">
                    @csrf
                    <input type="hidden" name="order_id" class="order_id" value="{{$order->id}}">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-form-label">Изделия</label>
                            <select class="form-control product" name="product" id="product" required>
                                @if(is_object($product))
                                    @foreach($product as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Название</label>
                            <input name="name" maxlength="250" class="form-control name" type="text" placeholder="Имя"
                                   id="example-text-input" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Длина</label>
                            <input name="length" max="9999999999" min="0" class="form-control length" type="number"
                                   step="any"
                                   placeholder="123"
                                   id="example-text-input" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Ширина</label>
                            <input name="width" max="9999999999" min="0" class="form-control width" type="number"
                                   step="any"
                                   placeholder="123"
                                   id="example-text-input" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Высота</label>
                            <input name="height" max="9999999999" min="0" class="form-control height" type="number"
                                   step="any"
                                   placeholder="123"
                                   id="example-text-input" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Глубина</label>
                            <input name="glubina" max="9999999999" min="0" class="form-control glubina" type="number"
                                   step="any"
                                   placeholder="123"
                                   id="example-text-input" value="" required>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Фасон</label>
                            <select class="form-control style" name="style" id="style" required>
                                @if(is_object($style))
                                    @foreach($style as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Цвет материала</label>
                            <select class="form-control materialColor" name="materialColor" id="materialColor" required>
                                @if(is_object($materialColors))
                                    @foreach($materialColors as $item)
                                        <option value="{{$item->id}}">{{$item->name.' '.$item->material->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Цвет каркаса</label>
                            <select class="form-control color" name="color" id="materialColor" required>
                                @if(is_object($colors))
                                    @foreach($colors as $item)
                                        <option value="{{$item->id}}">{{($item->name==1)?' ': $item->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Кол-во</label>
                            <input name="count" max="9999999999" min="0" class="form-control count" type="number"
                                   step="any"
                                   placeholder="123"
                                   id="example-text-input" value="" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-primary" id="btnSave">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <form method="post" action="" id="edit_form">
                    @method('PUT')
                    @csrf
                    <input type="hidden" class="id">
                    <input type="hidden" name="order_id" class="order_id" value="{{$order->id}}">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-form-label">Изделия</label>
                            <select class="form-control product" name="product" id="product" required>
                                @if(is_object($product))
                                    @foreach($product as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Название</label>
                            <input name="name" maxlength="250" class="form-control name" type="text" placeholder="Имя"
                                   id="example-text-input" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Длина</label>
                            <input name="length" max="9999999999" min="0" class="form-control length" type="number"
                                   step="any"
                                   placeholder="123"
                                   id="example-text-input" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Ширина</label>
                            <input name="width" max="9999999999" min="0" class="form-control width" type="number"
                                   step="any"
                                   placeholder="123"
                                   id="example-text-input" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Высота</label>
                            <input name="height" max="9999999999" min="0" class="form-control height" type="number"
                                   step="any"
                                   placeholder="123"
                                   id="example-text-input" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Глубина</label>
                            <input name="glubina" max="9999999999" min="0" class="form-control glubina" type="number"
                                   step="any"
                                   placeholder="123"
                                   id="example-text-input" value="" required>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Фасон</label>
                            <select class="form-control style" name="style" id="style" required>
                                @if(is_object($style))
                                    @foreach($style as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Цвет материала</label>
                            <select class="form-control materialColor" name="materialColor" id="materialColor" required>
                                @if(is_object($materialColors))
                                    @foreach($materialColors as $item)
                                        <option value="{{$item->id}}">{{$item->name.' '.$item->material->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Цвет каркаса</label>
                            <select class="form-control color" name="color" id="color" required>
                                @if(is_object($colors))
                                    @foreach($colors as $item)
                                        <option value="{{$item->id}}">{{($item->name==1)?' ': $item->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Кол-во</label>
                            <input name="count" max="9999999999" min="0" class="form-control count" type="number"
                                   step="any"
                                   placeholder="123"
                                   id="example-text-input" value="" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>
                        <button type="submit" class="btn btn-primary" id="btnEdit">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <form action="{{route('orderItem.index')}}" method="post" id="delete_form">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <p>Вы действительно хотите удалить.</p>
                        <input type="hidden" class="id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>
                        <button type="submit" class="btn btn-primary" id="btnDelete">Да</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#added').on('show.bs.modal', function (event) {
            var id=$('#added .product').val();
            var action = '{{route('product.index')}}';
            $.ajax({
                type: "GET",
                url: action + '/' + id,
                success: function (data) {
                    var modal = $('#added')
                    modal.find('.length').val(data.length);
                    modal.find('.width').val(data.width);
                    modal.find('.height').val(data.height);
                    modal.find('.glubina').val(data.glubina);
                    // $('.count').val(count);
                    modal.find('.name').val(data.name);
                    modal.find('#materialColor :selected').removeAttr('selected');
                    modal.find('#materialColor option[value=' + data.materialColor_id + ']').prop('selected', true);
                    modal.find('.style :selected').removeAttr('selected');
                    modal.find('.style option[value=' + data.style_id + ']').prop('selected', true);
                    modal.find('.product :selected').removeAttr('selected');
                    modal.find('.product option[value=' + data.id + ']').prop('selected', true);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
        $('#delete').submit(function () {
            var modal = $('#delete');
            var id = modal.find('.id').val();
            var action = '{{route('orderItem.index')}}';
            modal.find('form').attr('action', action + '/' + id);
        });
        $('#delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $('#delete');
            modal.find('.id').val(id);
            modal.find('.modal-title').text('Удаление ' + name);
        });
        $('#delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $('#delete');
            modal.find('.id').val(id);
            modal.find('.modal-title').text('Удаление ' + name);
        });
        $('#edit').submit(function () {
            var modal = $('#edit');
            var id = modal.find('.id').val();
            var action = '{{route('orderItem.index')}}';
            modal.find('form').attr('action', action + '/' + id);
        });
        $('#edit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var modal = $('#edit');
            var materialColor = button.data('materialcolor');
            var style = button.data('style');
            var length = button.data('length');
            var width = button.data('width');
            var height = button.data('height');
            var glubina = button.data('glubina');
            var color = button.data('color');
            var product = button.data('product');
            var count = button.data('count');
            modal.find('.id').val(id);
            modal.find('.length').val(length);
            modal.find('.width').val(width);
            modal.find('.height').val(height);
            modal.find('.glubina').val(glubina);
            modal.find('.count').val(count);
            modal.find('.name').val(name);
            modal.find('#materialColor :selected').removeAttr('selected');
            modal.find('#materialColor option[value=' + materialColor + ']').prop('selected', true);
            modal.find('.style :selected').removeAttr('selected');
            modal.find('.style option[value=' + style + ']').prop('selected', true);
            modal.find('.color :selected').removeAttr('selected');
            modal.find('.color option[value=' + color + ']').prop('selected', true);
            modal.find('.product :selected').removeAttr('selected');
            modal.find('.product option[value=' + product + ']').prop('selected', true);
            modal.find('.modal-title').text('Редактировать ' + name);
        });
    </script>

    <script>
        // $('select').change(function () {
        //     alert($(this).val());
        //     modal.find('select :selected').removeAttr('selected');
        //     modal.find('select option[value=' + $(this).val() + ']').prop('selected', true);
        // })
        $('#edit .product').change(function () {
            var id = $(this).val();
            var action = '{{route('product.index')}}';
            $.ajax({
                type: "GET",
                url: action + '/' + id,
                success: function (data) {
                    var modal = $('#edit')
                    modal.find('.length').val(data.length);
                    modal.find('.width').val(data.width);
                    modal.find('.height').val(data.height);
                    modal.find('.glubina').val(data.glubina);
                    // $('.count').val(count);
                    modal.find('.name').val(data.name);
                    modal.find('#materialColor :selected').removeAttr('selected');
                    modal.find('#materialColor option[value=' + data.materialColor_id + ']').prop('selected', true);
                    modal.find('.style :selected').removeAttr('selected');
                    modal.find('.style option[value=' + data.style_id + ']').prop('selected', true);
                    modal.find('.product :selected').removeAttr('selected');
                    modal.find('.product option[value=' + data.id + ']').prop('selected', true);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
        $('#added .product').change(function () {
            var id = $(this).val();
            var action = '{{route('product.index')}}';
            $.ajax({
                type: "GET",
                url: action + '/' + id,
                success: function (data) {
                    var modal = $('#added');
                    modal.find('.length').val(data.length);
                    modal.find('.width').val(data.width);
                    modal.find('.height').val(data.height);
                    modal.find('.glubina').val(data.glubina);
                    // $('.count').val(count);
                    modal.find('.name').val(data.name);
                    modal.find('#materialColor :selected').removeAttr('selected');
                    modal.find('#materialColor option[value=' + data.materialColor_id + ']').prop('selected', true);
                    modal.find('.style :selected').removeAttr('selected');
                    modal.find('.style option[value=' + data.style_id + ']').prop('selected', true);
                    modal.find('.product :selected').removeAttr('selected');
                    modal.find('.product option[value=' + data.id + ']').prop('selected', true);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $(function () {
            $('#btnSave').on('click', function () {
                $('#add_form').ajaxForm({
                    type: "post",
                    resetForm: true,
                    success: function (data) {
                        $('#added').modal('hide');
                        var materialTable = table(data);
                        $('#orderItem-crud').append(materialTable);
                    }
                });
            })
        });
        $('#btnEdit').on('click', function () {
            var modal = $('#edit');
            var id = modal.find('.id').val();
            var action = '{{route('orderItem.index')}}';
            modal.find('form').attr('action', action + '/' + id);
            $('#edit_form').ajaxForm({
                type: "PUT",
                resetForm: true,
                success: function (data) {
                    $('#edit').modal('hide');
                    var materialTable = table(data);
                    $("#orderItem_id_" + data.id).replaceWith(materialTable);
                }
            }).submit();
        });
        $('#btnDelete').on('click', function () {
            var modal = $('#delete');
            var id = modal.find('.id').val();
            var action = '{{route('orderItem.index')}}';
            modal.find('form').attr('action', action + '/' + id);
            $('#delete_form').ajaxForm({
                type: "DELETE",
                resetForm: true,
                success: function (data) {
                    $('#delete').modal('hide');
                    $("#orderItem_id_" + data).remove();
                }
            }).submit();
        });

        function table(data) {
            var table = '<tr id="orderItem_id_' + data.id + '"><th>' + data.id + '</th><td>' + data.name + '</td><td>' + data.length + '</td>' +
                '<td>' + data.width + '</td><td>' + data.height + '</td><td>' + data.glubina + '</td><td>' + data.style_name + '</td>' +
                '<td>' + data.material_name + '</td><td>' + data.materialColor_name + '</td><td>' + data.color + '</td><td>' + data.count + '</td>';
            table += '<td> <ul class="d-flex justify-content-center"> ' +
                '<li class="mr-3"><a href="' + data.http + '" class="text-secondary"><i class="fa fa-eye"></i></a></li>' +
                '<li class="mr-3"><a class="text-secondary" data-toggle="modal"' +
                ' data-target="#edit" data-id="' + data.id + '" data-name="' + data.name + '" data-materialColor="' + data.materialColor_id + '" ' +
                'data-material="' + data.material_id + '" data-style="' + data.style_id + '" data-length="' + data.length + '" ' +
                'data-width="' + data.width + '" data-height="' + data.height + '" data-glubina="' + data.glubina + '" ' +
                'data-color="' + data.color_id + '" data-count="' + data.count + '">';
            table += '<i class="fa fa-edit"></i></a></li> <li><a class="text-danger"><i class="ti-trash"';
            table += 'data-toggle="modal" data-target="#delete" data-id="' + data.id + '" data-name="' + data.name + '"></i></a></li></ul></td>';
            return table;
        }
    </script>
@endsection