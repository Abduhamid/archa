@extends('admin.layots.app')
@section('content')
        <div class="row">
            <div class="col-lg-3 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{$title}}</h4>
                        <button type="button" class="btn btn-primary mb-3 " data-toggle="modal" data-target="#detail">
                            Добавить файл
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">

                        <div class="invoice-area">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="invoice-head">
                                <span>Расчет стоимости материалов и комплектующих на {{date('d.m.Y', strtotime($order->date))}}</span>

                            </div>

                            @if(is_object($detail) && $detail->count()>=1 )
                                <div class="align-items-center">

                                    <h5>Заказ: {{$details->order}}</h5>
                                    <h5>Изделие: {{$orderItem->name}}</h5>
                                    <h5>Артикул изделия: </h5>
                                    <h5>Заказчик: {{$order->fio}}</h5>
                                    <h5>Разработчик: {{$details->developer}}</h5>
                                    <h5>Количество изделий в заказе: {{$orderItem->count}}</h5>
                                </div>
                                <div class="single-table">
                                    <div class="table-responsive">
                                        <table class="table table-hover progress-table text-center" id="orderItem-crud">
                                            <thead class="text-uppercase">
                                            <tr>
                                                <th scope="col">№</th>
                                                <th scope="col">Артикул</th>
                                                <th scope="col">Наименование материала</th>
                                                <th scope="col">Ед. изм.</th>
                                                <th scope="col">Расчетное количество</th>
                                                <th scope="col">Коэф-т</th>
                                                <th scope="col">Количество в изделии</th>
                                                <th scope="col">Количество в заказе</th>
                                                <th scope="col">Цена</th>
                                                <th scope="col">Стоимость в изделии</th>
                                                <th scope="col">Стоимость в заказе</th>
                                                <th scope="col">Примечание</th>
                                                {{--                                        <th scope="col">Действие</th>--}}
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($detail as $key=>$item)
                                                <tr id="orderItem_id_{{ $item->id }}">
                                                    <th scope="row">{{$key+1}}</th>
                                                    <td>{{$item->vendorcode}}</td>
                                                    <td>{{$item->name}}</td>
                                                    <td>{{$item->unitofme}}</td>
                                                    <td>{{$item->estimatedamount}}</td>
                                                    <td>{{$item->coefficent}}</td>
                                                    <td>{{$item->quantityinproduct}}</td>
                                                    <td>{{$item->quantityinorder}}</td>
                                                    <td>{{$item->price}}</td>
                                                    <td>{{$item->costinproduct}}</td>
                                                    <td>{{$item->costvalue}}</td>
                                                    <td>{{$item->note}}</td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    @endif
                                </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="modal fade" id="detail" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Добавление</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times</span></button>
                    </div>
                    <form method="post" action="{{route('detail', $orderItem->id)}}" id="add_form"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="custom-file">
                                    <input type="file" name="excel" class="custom-file-input" accept=".xlsx, .xls " id="inputGroupFile01">
                                    <label class="custom-file-label" for="inputGroupFile01">Выберите файл</label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            <button type="submit" class="btn btn-primary" id="btnSave">Добавить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

@endsection

@section('script')

@endsection