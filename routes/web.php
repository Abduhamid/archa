<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('color.index');
});

Route::prefix('/admin', ['middleware'=>'auth'])->group(function (){
    Route::resource('/order','Admin\OrderController');
    Route::resource('/orderItem','Admin\OrderItemController');
    Route::resource('/material','Admin\MaterialController');
    Route::resource('/materialColor','Admin\MaterialColorController');
    Route::resource('/style','Admin\StyleController');
    Route::resource('/color','Admin\ColorController');
    Route::resource('/product','Admin\ProductController');
    Route::get('/excelExport/{order}','Admin\OrderItemController@excelExport')->name('excelExport');
    Route::post('/detail/{orderItem}','Admin\DetailController@detail')->name('detail');

});
